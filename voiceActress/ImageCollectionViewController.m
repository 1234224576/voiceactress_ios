//
//  ImageCollectionViewController.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/31.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "ImageCollectionViewController.h"
#import <libxml/HTMLtree.h>
#import "HTMLparser.h"
#import "ImageCollectionCell.h"
#import "IDMPhotoBrowser.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface ImageCollectionViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate>
@property (retain,nonatomic) NSMutableArray *imageURLArr;
@property (strong,atomic) NSMutableArray *imageDataArr;

@property (retain,nonatomic) NSMutableArray *saveImageArr;//保存する予定の画像
@property (retain,nonatomic) NSMutableDictionary *indexpathDic;//保存する予定のセル
@property (nonatomic) BOOL selectImageFlag;//画像を選択可能にするかどうかのフラグ


@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ImageCollectionViewController

#define SELECT_MARK 1
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collectionView.allowsMultipleSelection = YES;
    // Do any additional setup after loading the view.
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.urlStr = appDelegate.imageListUrlStr;
    self.imageURLArr = [NSMutableArray new];
    self.imageDataArr = [NSMutableArray new];
    self.saveImageArr = [NSMutableArray new];
    self.indexpathDic = [NSMutableDictionary new];
    self.selectImageFlag = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self getImages];
    
//    長押し
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
        longPressGestureRecognizer.minimumPressDuration = 0.3;
    longPressGestureRecognizer.delegate = self;
    [self.collectionView addGestureRecognizer:longPressGestureRecognizer];
}

#pragma mark collection view delegate

-(IBAction)handleLongPressGesture:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"long press on table view");
    }else if (((UILongPressGestureRecognizer *)gestureRecognizer).state == UIGestureRecognizerStateBegan){
        NSArray *photos = [IDMPhoto photosWithImages:self.imageDataArr];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
        browser.displayActionButton = NO;
        [browser setInitialPageIndex:(indexPath.row)];
        [self presentViewController:browser animated:YES completion:nil];
    }
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.imageDataArr count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    if(indexPath.row  < self.imageDataArr.count){
        
        UIImageView *img = [[UIImageView alloc]initWithImage:self.imageDataArr[indexPath.row]];
        NSString *path = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
        
        if([self.indexpathDic objectForKey:path] == nil){
            [cell setBackgroundView:img];
            NSLog(@"LOAD_OFF_DATA");
        }
        else if([[self.indexpathDic objectForKey:path] row] == indexPath.row){
            
            UIImageView *selected = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"om_preserve_icon"]];
            selected.frame = CGRectMake(40,45, 30, 30);
            [img addSubview:selected];
            [img bringSubviewToFront:selected];
            [cell setBackgroundView:img];
            
            [self.saveImageArr addObject:img];
            NSLog(@"LOAD_ON_DATA:%ld",(long)indexPath.row);
        }
        
    }
    return cell;
}

-(void)getImages{
    
//    NSURL *url = [NSURL URLWithString:self.urlStr];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:self.urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self getImageList:responseObject];
        [self downloadImages:self.imageURLArr];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void) getImageList:(NSData*)htmlData{
    NSString *html = [[NSString alloc] initWithBytes:htmlData.bytes length:htmlData.length encoding:NSUTF8StringEncoding];
    NSError *error = nil;
    HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&error];
    HTMLNode *bodyNode = [parser body];
    NSArray *aNodes = [bodyNode findChildTags:@"img"];
    //ベースURLを取得
    NSURL *url = [NSURL URLWithString:self.urlStr];
    NSString *baseUrl = [NSString stringWithFormat:@"%@://%@/",[url scheme],[url host]];
    NSLog(@"base;%@",baseUrl);
    for (HTMLNode *node in aNodes) {
        NSString *str = [node getAttributeNamed:@"src"];
        if([str hasPrefix:@"http"]){
            
            if(str != nil){
                [self.imageURLArr addObject:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        else{
            NSString *newStr = [baseUrl stringByAppendingString:str];
            if(newStr != nil){
            [self.imageURLArr addObject:[newStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            }
        }
    }
    
}

- (void)downloadImages:(NSArray*)urls
{
    
    dispatch_queue_t queue = dispatch_queue_create("queue", DISPATCH_QUEUE_CONCURRENT);
    for (NSString *imgUrl in urls) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"画像をダウンロードします");
        dispatch_async(queue, ^{
        NSURL *url = [NSURL URLWithString:imgUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest : request returningResponse : &response error : &error ];
        if(data){
            UIImage *img = [UIImage imageWithData:data];
            if(img != nil){
                [self.imageDataArr addObject:img];
            }
            if ([NSThread isMainThread]) {
            } else {
                dispatch_sync (
                               dispatch_get_main_queue(),
                               ^{
                                [self.collectionView reloadData];
                               }
                               );
            }
        }else{
            NSLog(@"画像ロードエラー");
        }
        });
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.selectImageFlag = NO;
    [self selectedObjectAllRemove];
}
- (IBAction)tapSelectButton:(id)sender {
    if(!self.selectImageFlag){
        self.selectImageFlag = YES;
        self.selectButton.title = @"選択中";
    }else{
        self.selectImageFlag = NO;
        self.selectButton.title = @"選択";
        [self selectedObjectAllRemove];
    }
}


-(void)selectedObjectAllRemove{
    [self.saveImageArr removeAllObjects];
    [self.indexpathDic removeAllObjects];
    [self.collectionView reloadData];
}
- (IBAction)tapSaveButton:(id)sender {
    
    for(UIImageView *image in self.saveImageArr){
        UIImageWriteToSavedPhotosAlbum(
                                       image.image, self,nil, nil);
    }
    [self finishSaved];
}
-(void)finishSaved{
    UIAlertView *alert
    = [[UIAlertView alloc] initWithTitle:nil
                                 message:@"保存しました" delegate:nil
                       cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [alert show];
    self.selectImageFlag = NO;
    self.selectButton.title = @"選択";
    [self selectedObjectAllRemove];
}

// 選択の度に選択中のアイテムリストを NSLog に出力する
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if(self.selectImageFlag){
        NSString *key = [NSString stringWithFormat:@"%ld",indexPath.row];

        if([self.indexpathDic objectForKey:key] == nil)
            [self.indexpathDic setObject:indexPath forKey:key];
        else
            [self.indexpathDic removeObjectForKey:key];

        [self.saveImageArr removeAllObjects];
        [self.collectionView reloadData];
    }else{
        NSArray *photos = [IDMPhoto photosWithImages:self.imageDataArr];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
        browser.displayActionButton = NO;
        [browser setInitialPageIndex:(indexPath.row)];
        [self presentViewController:browser animated:YES completion:nil];
    }
    NSLog(@"-----------");
}

//UIImageView *selected = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"om_preserve_icon"]];
//selected.tag = SELECT_MARK;
//selected.frame = CGRectMake(40,45, 30, 30);
//
//UIImageView *imageView = [self.imageViewDataDic objectForKey:path];
//
//
//if([imageView viewWithTag:SELECT_MARK] != nil){
//    [[imageView viewWithTag:SELECT_MARK] removeFromSuperview];
//    [self.selectDataDic removeObjectForKey:path];
//    
//}else{
//    [imageView addSubview:selected];
//    
//    [self.selectDataDic setObject:num forKey:path];
//

@end
