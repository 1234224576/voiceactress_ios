//
//  CachedTableViewController.m
//  omoro
//
//  Created by TakafumiYANO on 2014/08/10.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "DetaliTableViewController.h"
#import "SignListData.h"
#import "CenterTableViewCell.h"
#import "WebVC.h"
#import "AppDelegate.h"

@interface DetaliTableViewController ()
@property(strong, nonatomic)SignListData *currentItem;
@property(strong,nonatomic) AppDelegate *appDelegate;
@property(nonatomic, strong)NSString *sinceDateStr;
@property(nonatomic, strong)NSMutableArray *dataArr;
@property(strong, nonatomic)NSDictionary *currentItemDic;

@end
@implementation DetaliTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataArr = [NSMutableArray array];
    NSLog(@"遷移成功：%@",self.name);

    [self fetchDataDic:self.name];
    
    self.tableView.rowHeight = 70.0;
 
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 70.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentItemDic = self.dataArr[indexPath.row];
    [self performSegueWithIdentifier:@"ArticleToWeb" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ArticleToWeb"] ) {
        WebVC *mWebVC = [segue destinationViewController];
        mWebVC.urlStr = self.currentItemDic[@"link"];
        mWebVC.mode = 0;
    }
}



- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenterCell" forIndexPath:indexPath];
	if (!cell) {
		cell = [[CenterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CenterCell"];
	}
    
    NSDictionary *content = self.dataArr[indexPath.row];
    if (![content[@"title"] isEqual:[NSNull null]])
    {
        cell.titleLabel.text = content[@"title"];
    }
    
    if (![content[@"media"] isEqual:[NSNull null]]){
       cell.mediaLabel.text = content[@"media"];
    }
    if (![content[@"published"] isEqual:[NSNull null]]){
        NSDate *pub = [self stringToNSDate:content[@"published"]];
        NSString *str = [self nsdateToNsstring:pub];
        cell.dateLabel.text = str;
    }
	return cell;
}


-(void)fetchDataDic:(NSString*)serchtext{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"isPopular", serchtext, @"keyword", nil];
    
    [manager GET:@"http://omoro.mobi/omoro/voiceactress/index.json" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *json_dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (json_dict) {
            if (![json_dict[@"feed"] isEqual:[NSNull null]])
            {
                NSMutableArray *currentItemArr =(NSMutableArray *)json_dict[@"feed"];
                int since  =[(NSString *)[json_dict objectForKey:@"until"] intValue];
                self.sinceDateStr = [NSString stringWithFormat:@"%d", (since +1)];
                [self.dataArr addObjectsFromArray:currentItemArr];
                [self.tableView reloadData];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"ネットワークオフライン" message:@"ネットワークが繋がる状態でリロードしてください。"];
        [alert bk_addButtonWithTitle:@"OK" handler:nil];
        [alert show];
    }];
    
}

-(NSDate*)stringToNSDate:(NSString*)str
{
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yy/MM/dd HH:mm:ss";
    NSDate* date = [formatter dateFromString:str];
    return date;
}

- (NSString *)nsdateToNsstring:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy/MM/dd HH:mm:ss"];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}




@end
