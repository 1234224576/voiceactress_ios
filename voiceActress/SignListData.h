//
//  CachedFeedData.h
//  omoro
//
//  Created by TakafumiYANO on 2014/08/10.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SignListData : NSManagedObject


@property (nonatomic, retain) NSString * name;

@end
