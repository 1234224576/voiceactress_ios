//
//  SettingTableViewController.m
//  omoro
//
//  Created by TakafumiYANO on 2014/08/09.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "SettingTableViewController.h"
#import <MessageUI/MessageUI.h>
#import "WebVC.h"

@interface SettingTableViewController ()<UIActionSheetDelegate,MFMailComposeViewControllerDelegate>
@property(strong, nonatomic)NSString* url;
@end

@implementation SettingTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSLog(@"%@",indexPath);
    if (indexPath.row ==0){
        if(indexPath.section ==0){
            [self sendMail];
        }else{
            //プライバシーポリシー
            self.url = @"http://omoro.mobi/omoro/privacypolicy/index";
            [self performSegueWithIdentifier:@"settingToWebView" sender:self];
        }
    }else{
            //利用規約
            self.url = @"http://omoro.mobi/omoro/kiyaku/index";
            [self performSegueWithIdentifier:@"settingToWebView" sender:self];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"settingToWebView"] ) {
        WebVC *mWebVC = [segue destinationViewController];
        mWebVC.urlStr = self.url;
        mWebVC.mode = 3;
    }
}



#pragma  mark メール用のメソッド

-(void)sendMail{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    
    // メール本文を設定
    [mailPicker setMessageBody:@""isHTML:NO];
    [mailPicker setToRecipients:@[@"info@cellars.co.jp"]];
    
    // 題名を設定
    [mailPicker setSubject:@"こえたいぷに対するご意見"];
    
    // メール送信用のモーダルビューを表示
    [self presentViewController:mailPicker animated:YES completion:nil];
}

// メール送信処理完了時のイベント
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result){
        case MFMailComposeResultCancelled:  // キャンセル
            break;
        case MFMailComposeResultSaved:      // 保存
            break;
        case MFMailComposeResultSent:       // 送信成功
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"送信に成功しました" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:     // 送信に失敗した場合
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"送信に失敗しました" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
