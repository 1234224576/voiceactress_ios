//
//  ListTableViewController.m
//  voiceActress
//
//  Created by 曽和修平 on 2014/11/08.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "ListTableViewController.h"
#import "CenterTableViewCell.h"
#import "FavoriteFeedData.h"
#import "AppDelegate.h"
#import "WebVC.h"
#import "SignListData.h"
#import "DetaliTableViewController.h"

@interface ListTableViewController ()
@property(strong, nonatomic)NSMutableArray *allNameData;
@property(strong, nonatomic)SignListData *currentItem;
@property(strong,nonatomic)AppDelegate *appDelegate;

@end

@implementation ListTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.allNameData = [[NSMutableArray alloc]initWithArray:[SignListData MR_findAll]];
    self.tableView.rowHeight = 70.0;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 50.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [SignListData MR_findAll].count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenterCell" forIndexPath:indexPath];
	if (!cell) {
		cell = [[CenterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CenterCell"];
	}
    
    if ( (indexPath.row) < [self.allNameData count]){
        self.currentItem = self.allNameData[indexPath.row];
        cell.titleLabel.text = self.currentItem.name;
        
    }
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentItem = self.allNameData[indexPath.row];
    NSLog(@"currentName:%@",self.currentItem.name);
    [self performSegueWithIdentifier:@"ListToArticle" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"ListToArticle"] ) {
        DetaliTableViewController *dtView = [segue destinationViewController];
        dtView.name = self.currentItem.name;
    }
}

- (NSString *)nsdateToNsstring:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy/MM/dd HH:mm:ss"];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

- (IBAction)tapEditButton:(id)sender
{
    if(self.tableView.editing){
        [self.tableView setEditing:NO animated:YES];
    }else{
        [self.tableView setEditing:YES animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSLog(@"%@",indexPath);
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            // Delete the row from the data source
            //cellの削除
            SignListData *delData = [self.allNameData objectAtIndex:indexPath.row];
    
            NSLog(@"%@",self.currentItem.name);
            //recordの削除
            [delData MR_deleteEntity];
            self.appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
            NSManagedObjectContext *context = self.appDelegate.context;
            [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                NSLog(@"success=%d, error=%@",success,error);
            }];


            [self.allNameData removeObjectAtIndex:indexPath.row];
            
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            
        } else if (editingStyle == UITableViewCellEditingStyleInsert) {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

@end
