//
//  FeedInfo.h
//  omoro
//
//  Created by TakafumiYANO on 2014/08/16.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FeedInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * isfavorite;
@property (nonatomic, retain) NSNumber * isCashed;
@property (nonatomic, retain) NSDecimalNumber * feed_id;

@end
