//
//  FavoriteFeedData.m
//  omoro
//
//  Created by TakafumiYANO on 2014/08/16.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "FavoriteFeedData.h"


@implementation FavoriteFeedData

@dynamic created;
@dynamic fb_share;
@dynamic hatebu;
@dynamic img_url;
@dynamic published;
@dynamic summary;
@dynamic title;
@dynamic tw_share;
@dynamic media;
@dynamic link;
@dynamic uid;

@end
