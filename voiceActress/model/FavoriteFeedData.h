//
//  FavoriteFeedData.h
//  omoro
//
//  Created by TakafumiYANO on 2014/08/16.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FavoriteFeedData : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * fb_share;
@property (nonatomic, retain) NSNumber * hatebu;
@property (nonatomic, retain) NSString * img_url;
@property (nonatomic, retain) NSDate * published;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * tw_share;
@property (nonatomic, retain) NSString * media;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSDecimalNumber * uid;

@end
