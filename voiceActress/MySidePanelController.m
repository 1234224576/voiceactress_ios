//
//  MySidePanelController.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/11.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "MySidePanelController.h"
#import "UIViewController+JASidePanel.h"

@interface MySidePanelController ()

@end

@implementation MySidePanelController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



-(void) awakeFromNib
{
    self.leftGapPercentage = 0.67;
    self.bounceOnSidePanelOpen = NO;
    [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"leftViewController"]];
//    self.rightGapPercentage = 1.0;
//    [self setRightPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"rightViewController"]];
    [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"centerViewController"]];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
