//
//  SearchVC.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "SearchVC.h"
#import <UIViewController+ScrollingNavbar.h>
#import "CenterTableViewCell.h"
#import "AppDelegate.h"
#import "WebVC.h"
#import "SignListData.h"
#import <SVProgressHUD/SVProgressHUD.h>

#import "UITableViewSub.h"

@interface SearchVC ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic, strong)NSString *sinceDateStr;
@property(strong,nonatomic) NSDictionary *currentItemDic;
@property(strong,nonatomic) AppDelegate *appDelegate;

@property(nonatomic, strong)NSMutableArray *dataArr;
@end

@implementation SearchVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rowButtonAction:)];
    longPressRecognizer.allowableMovement = 15;
    longPressRecognizer.minimumPressDuration = 0.6f;
    [self.tableView addGestureRecognizer: longPressRecognizer];

    self.dataArr = [NSMutableArray new];
    self.tableView.rowHeight = 70.0;
    self.tableView.allowsSelection = YES;
    self.tableView.allowsSelectionDuringEditing = YES;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    NSLog(@"delegate:%@ dataSource:%@", self.tableView.delegate, self.tableView.dataSource);
	//[self followScrollView:self.tableView];
   
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.searchBar becomeFirstResponder];
}

- (void) searchBarSearchButtonClicked: (UISearchBar *) searchBar {
	[searchBar resignFirstResponder];
	[self searchItem:searchBar.text];
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *) searchText {
 	[self searchItem:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    //Cancel時の処理を実装.
    [searchBar resignFirstResponder];  //元のViewに戻る.
}

-(void)fetchDataDic:(NSString*)serchtext{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"isPopular", serchtext, @"keyword", nil];
    
    [manager GET:@"http://omoro.mobi/omoro/voiceactress/index.json" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"responseObject: %@", responseObject);
        NSDictionary *json_dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (json_dict) {
            if (![json_dict[@"feed"] isEqual:[NSNull null]])
            {
                NSMutableArray *currentItemArr =(NSMutableArray *)json_dict[@"feed"];
                int since  =[(NSString *)[json_dict objectForKey:@"until"] intValue];
                self.sinceDateStr = [NSString stringWithFormat:@"%d", (since +1)];
                [self.dataArr addObjectsFromArray:currentItemArr];
                [self.tableView reloadData];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"ネットワークオフライン" message:@"ネットワークが繋がる状態でリロードしてください。"];
        [alert bk_addButtonWithTitle:@"OK" handler:nil];
        [alert show];
    }];

}

- (void) searchItem:(NSString *) searchText {
    
    [self.dataArr removeAllObjects];
    [self.tableView reloadData];
    [self fetchDataDic:searchText];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenterCell"];
	if (!cell) {
		cell = [[CenterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CenterCell"];
    }

    NSDictionary *content;
    if(self.dataArr[indexPath.row] != [NSNull null]){
        content = self.dataArr[indexPath.row];
    }
    if (![content[@"title"] isEqual:[NSNull null]])
    {
        cell.titleLabel.text = content[@"title"];
    }
  
    
    
    if(![content[@"media"] isEqual:[NSNull null]])
        cell.mediaLabel.text = content[@"media"];
    //[cell.wineMainImg sd_setImageWithURL:url placeholderImage:nil];
    cell.dateLabel.text=@"";
    
    
//    omoro[60558:60b] KEY:link
//    2014-08-26 01:36:18.077 omoro[60558:60b] KEY:title
//    2014-08-26 01:36:18.077 omoro[60558:60b] KEY:ogpimage
//    2014-08-26 01:36:18.077 omoro[60558:60b] KEY:point
//    2014-08-26 01:36:18.078 omoro[60558:60b] KEY:media
    
	return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.dataArr count];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.currentItemDic = self.dataArr[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; // 選択状態の解除
    [self performSegueWithIdentifier:@"serachToWeb" sender:self];
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 70.0;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepare");
    if ( [[segue identifier] isEqualToString:@"serachToWeb"] ) {
        WebVC *mWebVC = [segue destinationViewController];
        mWebVC.urlStr = self.currentItemDic[@"link"];
        mWebVC.mode = 4;
        mWebVC.currentPageDic = self.currentItemDic;
    }
}


-(IBAction)rowButtonAction:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil){
    }else if (((UILongPressGestureRecognizer *)gestureRecognizer).state == UIGestureRecognizerStateBegan){
        self.currentItemDic = self.dataArr[indexPath.row];
        if(![[self.currentItemDic allKeys]containsObject:@"ad_type"]){
            
            if([UIAlertController class]){
                //ios8用
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@をマイ声優に登録しますか？",self.currentItemDic[@"media"]] message:@"" preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
                    if([SignListData MR_findByAttribute:@"name" withValue:self.currentItemDic[@"media"]].count != 0){
                        [SVProgressHUD showSuccessWithStatus:@"既に登録されています"];
                    }else{
                        SignListData *sd = [SignListData MR_createEntity];
                        sd.name = self.currentItemDic[@"media"];
                        [magicalContext MR_saveOnlySelfAndWait];
                        [SVProgressHUD showSuccessWithStatus:@"登録完了！"];
                    }
                    
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
            }else{
                UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:nil message:[NSString stringWithFormat:@"%@をマイ声優に登録しますか？",self.currentItemDic[@"media"]]];
                [alert bk_addButtonWithTitle:@"OK" handler:^() {
                    NSLog(@"マイ声優に登録しますか？");
                    NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
                    if([SignListData MR_findByAttribute:@"name" withValue:self.currentItemDic[@"media"]].count != 0){
                        [SVProgressHUD showSuccessWithStatus:@"既に登録されています"];
                    }else{
                        SignListData *sd = [SignListData MR_createEntity];
                        sd.name = self.currentItemDic[@"media"];
                        [magicalContext MR_saveOnlySelfAndWait];
                        [SVProgressHUD showSuccessWithStatus:@"登録完了！"];
                    }
                    
                }];
                [alert bk_addButtonWithTitle:@"キャンセル" handler:nil];
                [alert show];
                
            }
            
        }
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
