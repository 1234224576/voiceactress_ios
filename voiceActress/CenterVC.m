//
//  CenterVC.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "CenterVC.h"
#import <UIViewController+ScrollingNavbar.h>
#import <KoaPullToRefresh/KoaPullToRefresh.h>
#import "CenterTableViewCell.h"
#import "UIViewController+JASidePanel.h"
#import "JASidePanelController.h"
#import "AppDelegate.h"
#import "IMNativeDelegate.h"
#import "IMNative.h"
#import "WebVC.h"
#import "SignListData.h"
#import "ASIWebPageRequest.h"
#import "ASIDownloadCache.h"
#import "Feedinfo.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ShareKit.h"
#import "SHKTwitter.h"
#import <Social/Social.h>
#import "MBProgressHUD.h"
#import "NADView.h"
@import Social;
@import Social.SLServiceTypes;


@interface CenterVC ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, IMNativeDelegate,appCMatchAppDelegate,NADViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic) CGFloat beginScrollOffsetY;
@property(nonatomic, strong)NSMutableArray *dataArr;
@property(nonatomic, strong)NSMutableArray *adArr;
@property(nonatomic, strong)NSMutableArray *feedArr;
@property(nonatomic, strong)NSString *sinceDateStr;
@property(nonatomic)NSInteger *webViewLoads;
@property(strong, nonatomic)NSDictionary *currentItemDic;
@property(retain, nonatomic)ASIWebPageRequest *request;
@property(strong, nonatomic)WSCoachMarksView *coachView;
@property(nonatomic)NSInteger requestMode; //0:新着 1:今日 2今週 3今月

//Nend
@property(nonatomic,retain) NADView *nadView;


//appCloud 関連
@property(nonatomic, strong)NSMutableArray *addArr;
@property(nonatomic)NSUInteger numAd;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightNavButton;

@end

@implementation CenterVC
- (void)viewDidLoad
{
    [super viewDidLoad];
    //インジケータ表示
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //Nend関連
    self.nadView =[[NADView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-112, 320, 50)];
    [self.nadView setIsOutputLog:YES];
    [self.nadView setNendID:@"9a0c6312c4579dde706d4416770a83ac65344e58" spotID:@"274285"];
    [self.nadView setDelegate:self];
    [self.nadView load];

    self.tableView.separatorColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    [self customNav];
    [self configPullToRefresh];
    //キャッシュ関連の初期設定
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rowButtonAction:)];
    longPressRecognizer.allowableMovement = 15;
    longPressRecognizer.minimumPressDuration = 0.6f;
    [self.tableView addGestureRecognizer: longPressRecognizer];
    
    //GA
    self.screenName = @"CenterMenu";
    // 左パネルからの通知を受信する設定
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(__leftSelectCellRowNotification:) name:kLeftViewDidSelectedCellNotification object:nil];
	[nc addObserver:self selector:@selector(__swipePanGestureBegin:) name:kPanGEsture object:nil];
    
//    in-mobiの設定
//    self.nativeAd = [[IMNative alloc] initWithAppId:INMOBIAD_ID];
//    self.nativeAd.delegate = self;
//    [self.nativeAd loadAd];
    self.addArr = [NSMutableArray new];
    [appCCloud setupAppCWithMediaKey:APPCLOUD_ID option:APPC_CLOUD_AD];
    //広告データの取得
    [appCCloud matchAppStartWithDelegate:self count:ADNUM];
    
//ヘルプの導入
    [self setUpHelp];
    
    self.requestMode = 0;//デフォルトの表示は新着の記事
    self.dataArr = [NSMutableArray new];
    self.feedArr = [NSMutableArray new];
    
  
}

#pragma mark ヘルプ関連
- (void)setUpHelp
{
    NSArray *marks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,0},{0,0}}],
                           @"caption": @"画面端のスワイプか左上のボタンでメニューを開けます。"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,160.0f},{320.0f,400.0f}}],
                           @"caption": @"記事を長押しでお気に入りの声優を登録できます。"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,160.0f},{320.0f,400.0f}}],
                           @"caption": @"好きな声優の記事をチェックしましょう！"
                           }
                       ];
    
    self.coachView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:marks];
    [self.view addSubview:self.coachView];
}
- (void)viewDidAppear:(BOOL)animated {
   if (![[NSUserDefaults standardUserDefaults] boolForKey:@"CenterVCHelpShowed"])
    {
        // 初回起動時
        [self.coachView performSelector:@selector(start) withObject:nil afterDelay:1.0f];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CenterVCHelpShowed"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    self.feedArr = [NSMutableArray arrayWithArray:[FeedInfo MR_findAll]];
    [self.view addSubview:self.nadView];
    [self.view bringSubviewToFront:self.nadView];
}


- (void)viewDidUnload {
    [appCCloud matchAppStop];
    [super viewDidUnload];
}
#pragma mark app cloud delegate 

- (void)onGetMatchAppWithIndex:(NSInteger)index appName:(NSString *)app_name description:(NSString *)description caption:(NSString *)caption icon:(UIImage *)icon banner:(UIImage *)banner;
{
    NSLog(@"アプリ名 : %@",description);
    NSMutableDictionary *mAdArr = [NSMutableDictionary new];
    mAdArr[@"appName"] = app_name;
    mAdArr[@"description"] = description;
    mAdArr[@"caption"] = caption;
    mAdArr[@"icon"] = icon;
    [self.addArr addObject:mAdArr];
    if(index == (ADNUM -1)){
        [self.tableView.pullToRefreshView startAnimating];
    }
}

#pragma  mark マイ声優登録処理の実装

-(IBAction)rowButtonAction:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil){
    }else if (((UILongPressGestureRecognizer *)gestureRecognizer).state == UIGestureRecognizerStateBegan){
        self.currentItemDic = self.dataArr[indexPath.row];
        if(![[self.currentItemDic allKeys]containsObject:@"ad_type"]){

            if([UIAlertController class]){
                //ios8用
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@をマイ声優に登録しますか？",self.currentItemDic[@"media"]] message:@"" preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
                    if([SignListData MR_findByAttribute:@"name" withValue:self.currentItemDic[@"media"]].count != 0){
                        [SVProgressHUD showSuccessWithStatus:@"既に登録されています"];
                    }else{
                        SignListData *sd = [SignListData MR_createEntity];
                        sd.name = self.currentItemDic[@"media"];
                        [magicalContext MR_saveOnlySelfAndWait];
                        [SVProgressHUD showSuccessWithStatus:@"登録完了！"];
                    }
                    
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
            }else{
                UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:nil message:[NSString stringWithFormat:@"%@をマイ声優に登録しますか？",self.currentItemDic[@"media"]]];
                [alert bk_addButtonWithTitle:@"OK" handler:^() {
                    NSLog(@"マイ声優に登録しますか？");
                    NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
                    if([SignListData MR_findByAttribute:@"name" withValue:self.currentItemDic[@"media"]].count != 0){
                        [SVProgressHUD showSuccessWithStatus:@"既に登録されています"];
                    }else{
                        SignListData *sd = [SignListData MR_createEntity];
                        sd.name = self.currentItemDic[@"media"];
                        [magicalContext MR_saveOnlySelfAndWait];
                        [SVProgressHUD showSuccessWithStatus:@"登録完了！"];
                    }
                    
                }];
                [alert bk_addButtonWithTitle:@"キャンセル" handler:nil];
                [alert show];

            }

        }
    }
}


- (void)__leftSelectCellRowNotification:(NSNotification *)notification
{
	NSDictionary *dict = [notification userInfo];
	NSLog(@"%@",dict[kSelectCellRowTitleUserInfoKey]);
    NSString *selStr = (NSString*)dict[kSelectCellRowTitleUserInfoKey];
    int selectIntval = selStr.intValue;
    
    switch (selectIntval) {
        case 1:
            //新着
            self.requestMode = 0;
            [self refreshTable];
            break;
        case 2:
            //人気記事今日
            self.requestMode = 1;
            [self refreshTable];
            break;
        case 3:
            //人気記事今週
            self.requestMode = 2;
            [self refreshTable];
            break;
        case 4:
            //人気記事今月
            self.requestMode = 3;
            [self refreshTable];
            break;
        case 5:
            //マイ声優記事
            [self performSegueWithIdentifier:@"centerToCache" sender:self];
            break;

        case 6:
            //マイ声優一覧
            [self performSegueWithIdentifier:@"CenterToList" sender:self];
            break;
            
        case 7:
            //お気に入り
            [self performSegueWithIdentifier:@"centerToFavorite" sender:self];
            break;

        case 8:
            //検索
            [self performSegueWithIdentifier:@"CenterMenuToSearch" sender:self];
            break;
        case 9:
            //設定
            [self performSegueWithIdentifier:@"centerToSetting" sender:self];
            break;
        default:
            break;
    }
}

- (void)__swipePanGestureBegin:(NSNotification *)notification
{
	NSDictionary *dict = [notification userInfo];
    if([dict[kPanGEsture]  isEqual: @YES]){
        self.tableView.scrollEnabled = NO;
    }else{
        self.tableView.scrollEnabled = YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentItemDic = self.dataArr[indexPath.row];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [MagicalRecord setupCoreDataStack];
    NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
    FeedInfo* ff = [FeedInfo MR_createEntity];
    ff.feed_id = [NSDecimalNumber decimalNumberWithString:(NSString*)self.currentItemDic[@"id"]];
    [magicalContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"success=%d, error=%@",success,error);
    }];
    [self performSegueWithIdentifier:@"CenterToWeb" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //Segueの特定
    if ( [[segue identifier] isEqualToString:@"CenterToWeb"] ) {
        WebVC *mWebVC = [segue destinationViewController];
        mWebVC.urlStr = self.currentItemDic[@"link"];
        mWebVC.mode = 0;
        mWebVC.currentPageDic = self.currentItemDic;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.dataArr count];
}


-(void)socialPostWithServiceType:(NSString*)serviceType{
    [[Partytrack sharedInstance] sendEventWithID:18696];
    SLComposeViewController * composeVC;
    NSString *urlStr = [NSString stringWithFormat:@"声優のことならこえたいぷ！全女性声優のブログが集結！"];
    composeVC = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    if (!composeVC) {
        SHKItem * item = [SHKItem text:urlStr];
        [SHKTwitter shareItem:item];
        return;
    }
    SLComposeViewControllerCompletionHandler completionHandle = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:NO forKey:@"permitImageListFun"];
            [ud synchronize];
        }else{
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:YES forKey:@"permitImageListFun"];
            [ud synchronize];
        }
        [composeVC dismissViewControllerAnimated:YES completion:nil];
    };
    composeVC.completionHandler = completionHandle;
    [composeVC setInitialText:urlStr];
//    NSURL *url = [NSURL URLWithString:@"http://a4.mzstatic.com/us/r30/Purple5/v4/6e/11/8b/6e118b99-79a9-e242-877c-1f2c24161b51/screen1136x1136.jpeg"];
//    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageNamed:@"om_icon"];
    [composeVC addImage:image];//写真追加
    [self presentViewController:composeVC animated:YES completion:nil];
}

- (void)refreshTable
{
    self.dataArr = [NSMutableArray new];
    self.sinceDateStr = @"0";
    [self.tableView.pullToRefreshView performSelector:@selector(stopAnimating) withObject:nil afterDelay:0];
    [self fetchData:@"0"];
}

-(NSDictionary*)setRequestMode:(NSInteger)mode withSinceStr:(NSString*)sinceStr
{
    NSDictionary *params;
    switch (mode) {
        case 0:
            //新着
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"20", @"limit", sinceStr, @"offset",nil];
            break;
        case 1:
            //今日
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"20", @"limit", sinceStr, @"offset", @"1", @"isPopular", @"0", @"kindPopular", nil];
            break;
        case 2:
            //今週
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"20", @"limit", sinceStr, @"offset", @"1", @"isPopular", @"1", @"kindPopular", nil];
            break;
        case 3:
            //今月
            params = [NSDictionary dictionaryWithObjectsAndKeys:@"20", @"limit", sinceStr, @"offset", @"1", @"isPopular", @"2", @"kindPopular", nil];
            break;
        default:
            break;
    }
    return params;
}


- (void)fetchData:(NSString*)sinceStr
{
    if(!sinceStr){
        sinceStr = @"0";
    }
    // APIからデータを取得する．
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *params = [self setRequestMode:self.requestMode withSinceStr:sinceStr];
    [manager GET:@"http://omoro.mobi/omoro/voiceactress/index.json" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject: %@", responseObject);
        NSDictionary *json_dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (json_dict) {
            if (![json_dict[@"feed"] isEqual:[NSNull null]])
            {
                NSMutableArray *currentItemArr =(NSMutableArray *)json_dict[@"feed"];
                int since  =[(NSString *)[json_dict objectForKey:@"until"] intValue];
                self.sinceDateStr = [NSString stringWithFormat:@"%d", (since +1)];
                //広告の混入
                int num =  (int)[currentItemArr count] -1;
                int pos = arc4random() % MIN(10,num);
                NSMutableDictionary *ad_dict = [NSMutableDictionary new];
                ad_dict[@"ad_type"] = [NSNumber numberWithInt:arc4random() % 5];
                if(![currentItemArr isEqual:[NSNull null]]){
                    appCReleaseStatus status = [appCCloud getReleaseStatus];
                    if(status != appCReleaseStatusInDevelopment){
                        [currentItemArr insertObject:ad_dict atIndex:pos];
                    }
                    [self.dataArr addObjectsFromArray:currentItemArr];
                    [self.tableView reloadData];
                }
            }
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"ネットワークオフライン" message:@"キャッシュに保存した記事は読めます。"];
        [alert bk_addButtonWithTitle:@"OK" handler:nil];
        [alert show];
    }];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //インジケータ閉じる
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    
	CenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenterCell" forIndexPath:indexPath];
	if (!cell) {
		cell = [[CenterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CenterCell"];
	}
    if ( (indexPath.row) < [self.dataArr count]){
        NSDictionary *currentItemDic = self.dataArr[indexPath.row];
        if([[currentItemDic allKeys]containsObject:@"ad_type"]){
            NSNumber *type = currentItemDic[@"ad_type"];
            NSDictionary *adDic = [self.addArr objectAtIndex:type.intValue];
            if (![adDic[@"caption"] isEqual:[NSNull null]])
            {
                cell.titleLabel.text = adDic[@"caption"];
            }
           
            cell.dateLabel.text = @"";
            if (![adDic[@"icon"] isEqual:[NSNull null]])
            {
                //cell.wineMainImg.image = adDic[@"icon"];
            }
            cell.mediaLabel.text = @"Sponsored";
            cell.isCachedView.image = nil;
            cell.overLayButton.enabled = YES;
            [appCCloud matchAppRegistWithControl:cell.overLayButton index:type.intValue];
        }else{
            cell.overLayButton.enabled = NO;
            //既読判定
            bool readFlag = NO;
            bool cachedFlag = NO;
            for (FeedInfo *feed in self.feedArr) {
                if([feed.feed_id compare:[NSDecimalNumber decimalNumberWithString:(NSString*)currentItemDic[@"id"]]] == NSOrderedSame)
                {
                    readFlag = YES;
                    if(feed.isCashed.boolValue){
                        cachedFlag = YES;
                    }
                }
            }
            if(readFlag)
            {
               cell.backgroundColor = [UIColor colorWithRed:0.9843137255 green:0.9843137255 blue:0.9725490196 alpha:1.0];
                cell.titleLabel.textColor = [UIColor colorWithRed:0.4980392157 green:0.4980392157 blue:0.4980392157 alpha:1.0];
            }else{
                cell.backgroundColor = [UIColor whiteColor];
                cell.titleLabel.textColor = [UIColor blackColor];
            }
            if(cachedFlag){
                cell.isCachedView.image = [UIImage imageNamed:@"om_preserve_icon"];
            }else{
                cell.isCachedView.image = nil;
            }

            if (![currentItemDic[@"title"] isEqual:[NSNull null]])
            {
                cell.titleLabel.text = currentItemDic[@"title"];
            }
            if (![currentItemDic[@"point"] isEqual:[NSNull null]])
            {
                cell.pointLabel.text = [NSString stringWithFormat:@"%@シェア",(NSString*)currentItemDic[@"point"]];
            }
        
            if (![currentItemDic[@"published"] isEqual:[NSNull null]])
            {
                
                NSDate *pub = [self stringToNSDate:currentItemDic[@"published"]];
                NSString *str = [self nsdateToNsstring:pub];
                cell.dateLabel.text = str;
                
                //未来の記事を非表示にする処理
                NSDate *now = [NSDate date];
                NSTimeInterval delta = [now timeIntervalSinceDate:pub];
                if(delta <= 0){
                    [self.dataArr removeObjectAtIndex:indexPath.row];
                    [self.tableView reloadData];
                }
            }
            if (![currentItemDic[@"media"] isEqual:[NSNull null]])
            {
                cell.mediaLabel.text = currentItemDic[@"media"];
            }
           // [cell.wineMainImg sd_setImageWithURL:[NSURL URLWithString:currentItemDic[@"ogpImage"]] placeholderImage:nil];
         
        }
    }
	return cell;
}

-(NSDate*)stringToNSDate:(NSString*)str
{
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    NSDate* date = [formatter dateFromString:str];
    return date;
}


- (NSString *)nsdateToNsstring:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy/MM/dd HH:mm:ss"];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

-(void)tableView:(UITableView*)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(([self.dataArr count] - indexPath.row) == 5 ){
        [self fetchData:self.sinceDateStr];
    }
}

- (void)dealloc
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self name:kLeftViewDidSelectedCellNotification object:nil];
}


-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 80.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


//引っ張り更新機能

- (void)configPullToRefresh
{
    [self.tableView addPullToRefreshWithActionHandler:^{
        [self refreshTable];
    } withBackgroundColor:[UIColor colorWithRed:0.164 green:0.164 blue:0.164 alpha:1.0]withPullToRefreshHeightShowed:5.0f];
    [self.tableView.pullToRefreshView setTextColor:[UIColor whiteColor]];
    [self.tableView.pullToRefreshView setFontAwesomeIcon:@"icon-refresh"];
    [self.tableView.pullToRefreshView setTitle:@"下げて更新" forState:KoaPullToRefreshStateStopped];
    [self.tableView.pullToRefreshView setTitle:@"指を話して更新" forState:KoaPullToRefreshStateTriggered];
    [self.tableView.pullToRefreshView setTitle:@"更新中..." forState:KoaPullToRefreshStateLoading];
    [self.tableView setShowsVerticalScrollIndicator:NO];
}

- (void)customNav
{
    //タイトルフォントの変更
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"AmericanTypewriter-Bold" size:15];
   // titleLabel.text = self.navigationItem.title;
    titleLabel.text = @"こえたいぷ";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel sizeToFit];
    
    self.navigationItem.titleView = titleLabel;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark in-mobi delegate method

-(void)nativeAdDidFinishLoading:(IMNative*)native {
    /* Use below lines only if you are using SDK 4.1.0 */
    const char* nativeCString = [native.content cStringUsingEncoding:NSISOLatin1StringEncoding];
    NSString* utf8PubContent = [[NSString alloc] initWithCString:nativeCString encoding:NSUTF8StringEncoding];
    NSData* data = [utf8PubContent dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error = nil;
    NSDictionary* jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSMutableDictionary* nativeJsonDict = [NSMutableDictionary dictionaryWithDictionary:jsonDict];
    if (error == nil && nativeJsonDict != nil) {
        [nativeJsonDict setValue:[NSNumber numberWithBool:YES] forKey:@"isAd"];
    }
    NSLog(@"広告の辞書%@",nativeJsonDict);
}
-(void)nativeAd:(IMNative*)native didFailWithError:(IMError*)error {
    NSLog(@"in-mobi広告ローディングエラー%@", error);
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


@end