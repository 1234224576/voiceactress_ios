//
//  AppDelegate.h
//  omoro
//
//  Created by TakafumiYANO on 2014/07/11.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, appCDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *imageListUrlStr;
@property (strong,nonatomic) NSManagedObjectContext *context;
@end
