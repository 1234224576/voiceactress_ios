//
//  ImageCollectionViewController.h
//  omoro
//
//  Created by TakafumiYANO on 2014/07/31.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewController : UIViewController
@property (strong,nonatomic) NSString *urlStr;
@end
