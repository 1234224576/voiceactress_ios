//
//  MySHKConfigurator.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "MySHKConfigurator.h"

@implementation MySHKConfigurator


- (NSString*)appName {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
}

// アプリケーションのURL
- (NSString*)appURL {
    return @"http://example.com/";
}

/*------------------------------------------------------------
 * 各サービスのAPIに接続するための設定
 *
 *   必要な定義は、以下のソースファイルから適宜コピーする。
 *   /Pods/ShareKit/DefaultSHKConfigurator.m
 *----------------------------------------------------------*/
// Twitter - http://dev.twitter.com/apps/new
/*
 Important Twitter settings to get right:
 
 Differences between OAuth and xAuth
 --
 There are two types of authentication provided for Twitter, OAuth and xAuth.  OAuth is the default and will
 present a web view to log the user in.  xAuth presents a native entry form but requires Twitter to add xAuth to your app (you have to request it from them).
 If your app has been approved for xAuth, set SHKTwitterUseXAuth to 1.
 
 Callback URL (important to get right for OAuth users)
 --
 1. Open your application settings at http://dev.twitter.com/apps/
 2. 'Application Type' should be set to BROWSER (not client)
 3. 'Callback URL' should match whatever you enter in SHKTwitterCallbackUrl.  The callback url doesn't have to be an actual existing url.  The user will never get to it because ShareKit intercepts it before the user is redirected.  It just needs to match.
 */

/*
 If you want to force use of old-style, pre-IOS5 twitter framework, for example to ensure
 twitter accounts don't end up in the devices account store, set this to true.
 */
- (NSNumber*)forcePreIOS5TwitterAccess {
    return [NSNumber numberWithBool:NO];
}
// Consumer key
- (NSString*)twitterConsumerKey {
    return @"siVR8gdbykTdTsbBpi3L4M2sn";
}
// Consumer secret
- (NSString*)twitterSecret {
    return @"UwKUlLfrL6KJDcc3pSPY2InJrmTubPZkcKBBei1ewL98kdRUkF";
}
// コールバックURL(xAuth認証の場合は不要)
- (NSString*)twitterCallbackUrl {
    return @"http://exmple.com";
}
// xAuth認証の場合は 1 にする
- (NSNumber*)twitterUseXAuth {
    return [NSNumber numberWithInt:0];
}
// アプリの認証時にアプリのTwitterアカウントのフォローをユーザーに尋ねる場合は以下にアカウント名を指定(xAuthの場合のみ)
- (NSString*)twitterUsername {
    return @"";
}

// Facebook - https://developers.facebook.com/apps
/* SHKFacebookAppID is the Application ID provided by Facebook
 SHKFacebookLocalAppID is used if you need to differentiate between several iOS apps running against a single Facebook app. Useful, if you have full and lite versions of the same app,
 and wish sharing from both will appear on facebook as sharing from one main app.
 You have to add different suffix to each version. Do not forget to fill both suffixes on facebook developer ("URL Scheme Suffix").
 Leave it blank unless you are sure of what you are doing.
 The CFBundleURLSchemes in your App-Info.plist should be "fb" + the concatenation of these two IDs.
 Example:
 SHKFacebookAppID = 555
 SHKFacebookLocalAppID = lite
 
 Your CFBundleURLSchemes entry: fb555lite
 */
// App ID
- (NSString*)facebookAppId {
    return @"367037703473961";
}
- (NSString*)facebookLocalAppId {
    return @"";
}

- (NSArray*)facebookWritePermissions {
    return [NSArray arrayWithObjects:@"publish_actions", nil];
}

/*
 If you want to force use of old-style, posting path that does not use the native sheet. One of the troubles
 with the native sheet is that it gives IOS6 props on facebook instead of your app. This flag has no effect
 on the auth path. It will try to use native auth if availible.
 */
- (NSNumber*)forcePreIOS6FacebookPosting {
	return [NSNumber numberWithBool:false];
    
    /*
     The default behavior (return NO from this function) causes user to be kind of locked in to use iOS settings.app credentials. If he has not Facebook credentials set in settings.app, the user is presented alert instructing him to add his credentials to settings.app if wants to share with Facebook. If you instead prefer your user to be automagically switched to legacy (Safari/Facebook app trip) authentication, use following implementation
     */
    
    /*
     BOOL result = NO;
     //if they have an account on their device, then use it, but don't force a device level login
     if (NSClassFromString(@"SLComposeViewController")) {
     result = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
     }
     return [NSNumber numberWithBool:result];
     */
}



@end
