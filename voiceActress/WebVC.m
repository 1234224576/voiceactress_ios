//
//  WebVC.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "WebVC.h"
#import "ShareKit.h"
#import "SHKTwitter.h"
#import <SHKFacebook.h>
#import <UIViewController+ScrollingNavbar.h>
#import "UIViewController+JASidePanel.h"
#import "JASidePanelController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AwesomeMenu.h"
#import "AwesomeMenuItem.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "ImageCollectionViewController.h"
#import "FavoriteFeedData.h"
#import "ASIDownloadCache.h"
@import Social;
@import Social.SLServiceTypes;


@interface WebVC () <UIWebViewDelegate, UIScrollViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate, AwesomeMenuDelegate>{
   
}
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property(nonatomic) CGFloat beginScrollOffsetY;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *imageViewButton;
@property (weak,nonatomic) NSString *articleTitle;
@property (nonatomic) AwesomeMenu *menu;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property(strong, nonatomic)WSCoachMarksView *coachView;


@end
@implementation WebVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
//    GA
    self.screenName = @"WEBVC";
    [self.navigationItem setTitle:@"                                   "];
//    スクロールを検知する
    self.webview.scrollView.delegate = self;
    self.backButton.image = [[UIImage imageNamed:@"om_back_icon_hidden"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.imageViewButton.image = [[UIImage imageNamed:@"om_photo_mode_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.forwardButton.image = [[UIImage imageNamed:@"om_next_button_hidden"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    // 横スワイプ動作
    UISwipeGestureRecognizer *swipe  = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    // スワイプの方向は右方向を指定する。
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    // スワイプ動作に必要な指は1本と指定する。
    swipe.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:swipe];
//    [self setUpHelp];
    //NavigationBarにボタンを追加
    // initWithBarButtonSystemItemに、表示したいアイコンを指定します。
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                   target:self
                                   action:@selector(shareSheet)];
    
    // ナビゲーションバーに追加する。
    self.navigationItem.rightBarButtonItem = leftButton;
}

-(void)shareSheet{
    
    Class class = NSClassFromString(@"UIAlertController");
    if(class){
        [self shareListForIos8];
    }else{
        [self shareListForIos7];
    }
}

-(void)shareListForIos7{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"なにで共有しますか？"
                                                             delegate:self
                                                    cancelButtonTitle:@"いいえ"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Twitter",@"Facebook",@"Line",@"メール",@"Safariで開く",@"不適切な内容を通報する",nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self twShare];
            break;
        case 1:
            [self fbShare];
            break;
        case 2:
            [self lineShare];
            break;
        case 3:
            [self mailingUrl];
            break;
        case 4:
            [self showSafari];
            break;
        case 5:
            [self reportMail];
            break;
    }
}

-(void)shareListForIos8{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"なにで共有しますか？" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self twShare];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self fbShare];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Line" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self lineShare];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"メール" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self mailingUrl];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Safariで開く" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self showSafari];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"不適切な内容を通報する" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self reportMail];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"キャンセル" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // cancelボタンが押された時の処理
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)reportMail{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    // メール本文を設定
    [mailPicker setMessageBody:[NSString stringWithFormat:@"（通報内容をお書き下さい）"] isHTML:YES];
    
    // 題名を設定
    [mailPicker setSubject:@"不適切な記事を通報"];
    
    //宛先を設定
    [mailPicker setToRecipients:[NSArray arrayWithObject:@"info@cellars.co.jp"]];
    
    // メール送信用のモーダルビューを表示
    [self presentViewController:mailPicker animated:YES completion:nil];
    
}


#pragma mark ヘルプ関連
- (void)setUpHelp
{
    NSArray *marks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{275,460},{45,45}}],
                           @"caption": @"面白い記事を簡単に友達に紹介できます。"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{115,460},{45,45}}],
                           @"caption": @"記事の画像を一覧で見れます。"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,160.0f},{320.0f,400.0f}}],
                           @"caption": @"前の画面ヘは左上のボタンをクリックまたは左端のスワイプで戻れます。"
                           }
                       ];
    
    self.coachView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:marks];
    [self.view addSubview:self.coachView];
}
- (void)viewDidAppear:(BOOL)animated {
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"WEBVCHelpShowed"];
    if (!coachMarksShown) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"WEBVCHelpShowed"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.coachView performSelector:@selector(start) withObject:nil afterDelay:1.0f];
    }
        [self loadWeb];
}


- (void)buildAwesomeMenu
{
    //画像設定
    //メニューのボタン
    AwesomeMenuItem *addMenuItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_share_button"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    AwesomeMenuItem *twitterItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_twitter"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    AwesomeMenuItem *lineItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_line"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    AwesomeMenuItem *fbItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_facebook"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    AwesomeMenuItem *favItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_favorote"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    AwesomeMenuItem *browItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_browser"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    AwesomeMenuItem *urlItem = [[AwesomeMenuItem alloc]initWithImage:[UIImage imageNamed:@"om_url"] highlightedImage:nil ContentImage:nil highlightedContentImage:nil];
    NSArray *MenuItemArray;
    if(self.mode == 2){
        urlItem.hidden = YES;
        browItem.hidden = YES;
        favItem.hidden = YES;
    }else if(self.mode == 1){
        favItem.hidden = YES;
    }else if(self.mode == 4){
//        twitterItem.hidden = YES;
//        fbItem.hidden = YES;
//        urlItem.hidden = YES;
    }
    else{
    }
        MenuItemArray = @[urlItem,browItem,favItem,fbItem,lineItem,twitterItem];
    self.menu = [[AwesomeMenu alloc] initWithFrame:self.view.bounds startItem:addMenuItem  optionMenus:MenuItemArray];
    self.menu.delegate = self;
    CGSize frameSize = self.view.bounds.size;
    self.menu.startPoint = CGPointMake(frameSize.width - 25, frameSize.height - 22.5f);
    self.menu.menuWholeAngle = - M_PI * 0.5 * [MenuItemArray count] / 6;
    self.menu.farRadius = 158.0f;
    self.menu.nearRadius = 158.0f;
    self.menu.endRadius = 158.0f;
    self.menu.timeOffset = 0.03f;
    self.menu.rotateAngle = 0;
    self.menu.tag = 1;
    if([self.view viewWithTag:1] ==nil){
        [self.view addSubview:self.menu];
    }else{
        NSLog(@"NILじゃないよ!!");
    }
}

- (void)loadWeb
{
    switch (self.mode) {
        case 2:
        {
            //キャッシュ読み込み
            NSString *response = [NSString stringWithContentsOfFile: self.urlStr encoding:NSUTF8StringEncoding error:nil];
            NSURL *baseURL = [NSURL URLWithString:self.urlStr];
            NSLog(@"%@",[baseURL absoluteString]);
            [self.webview loadHTMLString:response baseURL:baseURL];
            break;
        }
        default:
        {
            NSURL *url = [NSURL URLWithString:self.urlStr];
            NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:20];
            [self.webview stopLoading];
            [self.webview loadRequest:request];
            break;
        }
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    if(self.mode != 3){
        [self buildAwesomeMenu];
    }
}

#warning 明日以降ここを修正する
//メニュー用のdelegate method
- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx
{
    if(idx == 0){
        NSLog(@"MAIL LINK");
        [self mailingUrl];
    }else if(idx == 1){
        NSLog(@"SHOW SAFARI");
        [self showSafari];
    }else if(idx == 2){
        NSLog(@"FAVORITE");
        [self favorite];
        
    }else if(idx == 3){
        NSLog(@"FB SHARE");
        [self fbShare];
    }else if(idx == 4){
        NSLog(@"LINE SHARE");
        [self lineShare];
        
    }else if(idx == 5){
        NSLog(@"TWITTER SHARE");
        [self twShare];
    }
}

-(void)mailingUrl{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    NSString* title = [self.webview stringByEvaluatingJavaScriptFromString:@"document.title"];
    // メール本文を設定
    [mailPicker setMessageBody:[NSString stringWithFormat:@"%@<br><br><a href=\"%@\">%@</a><br><br><bt>好きな声優さんのブログをチェック！こえたいぷ<br>",title,self.urlStr,self.urlStr] isHTML:YES];
    
    // 題名を設定
    [mailPicker setSubject:_articleTitle];
    
    // メール送信用のモーダルビューを表示
    [self presentViewController:mailPicker animated:YES completion:nil];
}

// メール送信処理完了時のイベント
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result){
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"送信に成功しました" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        break;
        }
        case MFMailComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"送信に失敗しました" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)showSafari{
    NSURL *url = [NSURL URLWithString:self.urlStr];
    [[UIApplication sharedApplication]openURL:url];
}

-(void)favorite{
    
    if([UIAlertController class]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"お気に入りに登録しますか？" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [MagicalRecord setupCoreDataStack];
            NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
            if([FavoriteFeedData MR_findByAttribute:@"uid" withValue:[NSDecimalNumber decimalNumberWithString:self.currentPageDic[@"id"]]].count != 0){
                
                UIAlertController *alt = [UIAlertController alertControllerWithTitle:@"既に登録されています。" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction =
                [UIAlertAction actionWithTitle:@"OK"
                                         style:UIAlertActionStyleCancel
                                       handler:nil];
                [alt addAction:alertAction];
                [self presentViewController:alt animated:YES completion:nil];
                
            }else{
                FavoriteFeedData* ff = [FavoriteFeedData MR_createEntity];
                ff.title = self.currentPageDic[@"title"];
                ff.img_url = self.currentPageDic[@"ogpImage"];
                ff.media = self.currentPageDic[@"media"];
                ff.link = self.urlStr;
                ff.uid = [NSDecimalNumber decimalNumberWithString:self.currentPageDic[@"id"]];
                ff.created = [NSDate date];
                [magicalContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    NSLog(@"success=%d, error=%@",success,error);
                    
                    UIAlertController *alt = [UIAlertController alertControllerWithTitle:@"登録完了しました。" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction =
                    [UIAlertAction actionWithTitle:@"OK"
                                             style:UIAlertActionStyleCancel
                                           handler:nil];
                    [alt addAction:alertAction];
                    [self presentViewController:alt animated:YES completion:nil];
                    
                }];
            }

            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:nil message:@"お気に入りに登録しますか？"];
        [alert bk_addButtonWithTitle:@"OK" handler:^() {
            [MagicalRecord setupCoreDataStack];
            NSManagedObjectContext *magicalContext = [NSManagedObjectContext MR_defaultContext];
            if([FavoriteFeedData MR_findByAttribute:@"uid" withValue:[NSDecimalNumber decimalNumberWithString:self.currentPageDic[@"id"]]].count != 0){
                
                UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:nil message:@"既に登録されています。"];
                [alert bk_addButtonWithTitle:@"OK" handler:nil];
                [alert show];
                
            }else{
                FavoriteFeedData* ff = [FavoriteFeedData MR_createEntity];
                ff.title = self.currentPageDic[@"title"];
                ff.img_url = self.currentPageDic[@"ogpImage"];
                ff.media = self.currentPageDic[@"media"];
                ff.link = self.urlStr;
                ff.uid = [NSDecimalNumber decimalNumberWithString:self.currentPageDic[@"id"]];
                ff.created = [NSDate date];
                [magicalContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    NSLog(@"success=%d, error=%@",success,error);
                    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:nil message:@"登録完了しました。"];
                    [alert bk_addButtonWithTitle:@"OK" handler:nil];
                    [alert show];
                }];
            }
        }];
        [alert bk_addButtonWithTitle:@"キャンセル" handler:nil];
        [alert show];
    }
}

-(NSDate*)stringToNSDate:(NSString*)str
{
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    NSDate* date = [formatter dateFromString:str];
    return date;
}

-(void)lineShare{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
#warning アドレス要変更
    NSString *doc = [NSString stringWithFormat:@"%@ %@\n%@\n好きな声優さんのブログをチェック！こえたいぷ\n https://itunes.apple.com/us/app/koetaipu/id942084056?l=ja&ls=1&mt=8",self.currentPageDic[@"title"],self.currentPageDic[@"media"],self.urlStr];
        NSString *sendDoc = [doc stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [pasteboard setString:[NSString stringWithFormat:@"%@",sendDoc]];

    NSString *string = [NSString stringWithFormat:@"line://msg/text/%@",pasteboard.string];
    NSURL *url = [NSURL URLWithString:string];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark 戻るボタン等の実装

- (IBAction)tapImageViewButton:(id)sender {
    //機能解放の確認
        AppDelegate *appDelegete = [[UIApplication sharedApplication] delegate];
        appDelegete.imageListUrlStr = self.urlStr;
        [self performSegueWithIdentifier:@"WebVCtoPictureCell" sender:self];
}

- (void)twShareApp
{
    SLComposeViewController * composeVC;
    NSString *urlStr = [NSString stringWithFormat:@"好きな声優さんのブログをチェック！こえたいぷ"];
    composeVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    if (!composeVC) {
        SHKItem * item = [SHKItem text:urlStr];
        [SHKTwitter shareItem:item];
        return;
    }
    SLComposeViewControllerCompletionHandler completionHandle = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:NO forKey:@"permitImageListFun"];
            [ud synchronize];
        }else{
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setBool:YES forKey:@"permitImageListFun"];
            [ud synchronize];
        }
        [composeVC dismissViewControllerAnimated:YES completion:nil];
    };
    composeVC.completionHandler = completionHandle;
    [composeVC setInitialText:urlStr];
//    NSURL *url = [NSURL URLWithString:@"http://a4.mzstatic.com/us/r30/Purple5/v4/6e/11/8b/6e118b99-79a9-e242-877c-1f2c24161b51/screen1136x1136.jpeg"];
//    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageNamed:@"om_icon"];
    [composeVC addImage:image];//写真追加
    [self presentViewController:composeVC animated:YES completion:nil];
}


- (IBAction)tapBackButton:(UIBarButtonItem*)sender {
    [self.webview goBack];
}

- (IBAction)tapForwardButton:(id)sender {
    [self.webview goForward];
}

//ソーシャルシェア機能
- (void)fbShare
{
    [[Partytrack sharedInstance] sendEventWithID:18693];
    FBLinkShareParams *p = [[FBLinkShareParams alloc] init];
    NSString *urlStr = [NSString stringWithFormat:@"http://omoro.mobi/omoro/Share/index.php?id=%@&kind=4",self.currentPageDic[@"id"]];
    NSString* title = [self.webview stringByEvaluatingJavaScriptFromString:@"document.title"];
    p.link = [NSURL URLWithString:urlStr];
    BOOL canShareFB = [FBDialogs canPresentShareDialogWithParams:p];
    if(canShareFB){
        [FBDialogs presentShareDialogWithLink:[NSURL URLWithString:urlStr] name:@"好きな声優さんのブログをチェック！こえたいぷ" caption:title description:title picture:nil clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            if(error) {
                NSLog(@"Error: %@", error.description);
            } else {
                NSLog(@"Success!");
            }
        }];
    }else{
        //アプリ連携でシェア出来なかった場合の処理(sharekitでシェアを実施する)
        SHKItem *item = [SHKItem URL:[NSURL URLWithString:urlStr] title:@"好きな声優さんのブログをチェック！こえたいぷ" contentType:SHKURLContentTypeWebpage];
        [SHKFacebook shareItem:item];
    }
}

- (void)twShare
{
    [[Partytrack sharedInstance] sendEventWithID:18694];
    [self socialPostWithServiceType:SLServiceTypeTwitter];
}

-(void)socialPostWithServiceType:(NSString*)serviceType{
    SLComposeViewController * composeVC;
    NSString *urlStr = [NSString stringWithFormat:@"%@ %@ http://omoro.mobi/omoro/Share/index.php?id=%@&kind=4\n声優ブログまとめ こえたいぷ",self.currentPageDic[@"title"],self.currentPageDic[@"media"],self.currentPageDic[@"id"]];
    composeVC = [SLComposeViewController composeViewControllerForServiceType:serviceType];
    if (!composeVC) {
        SHKItem * item = [SHKItem text:urlStr];
        [SHKTwitter shareItem:item];
        return;
    }
    SLComposeViewControllerCompletionHandler completionHandle = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
        }else{
            //投稿を押した場合
        }
        [composeVC dismissViewControllerAnimated:YES completion:nil];
    };
    composeVC.completionHandler = completionHandle;
    [composeVC setInitialText:urlStr];
//    NSURL *url = [NSURL URLWithString:self.currentPageDic[@"ogpImage"]];
//    NSData *data = [NSData dataWithContentsOfURL:url];
//    UIImage *image = [UIImage imageWithData:data];
//    [composeVC addImage:image];//写真追加
    [self presentViewController:composeVC animated:YES completion:nil];
}


-(void)swipe:(UISwipeGestureRecognizer *)gesture {
//    [self.sidePanelController showCenterPanelAnimated:YES];
}


- (void)webViewDidStartLoad:(UIWebView*)webView
{
    
    NSString* title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [self.navigationItem setTitle:title];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}


- (void)webViewDidFinishLoad:(UIWebView*)webView
{
    if([self.webview canGoBack]){
        self.backButton.image = [[UIImage imageNamed:@"om_back_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.backButton.enabled = YES;
    }else{
        self.backButton.image = [[UIImage imageNamed:@"om_back_icon_hidden"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.backButton.enabled = NO;
    }
    if([self.webview canGoForward]){
        self.forwardButton.image = [[UIImage imageNamed:@"om_next_button"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.forwardButton.enabled = YES;
    }else{
        self.forwardButton.image = [[UIImage imageNamed:@"om_next_button_hidden"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.forwardButton.enabled = NO;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    // リンクがクリックされたとき
    if (navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeOther) {
        NSLog(@"%@",[request URL]);
        // Twitterページか?
        if ([self isTwitterURL:[request URL]]) {
            [self twShare];
            return NO;
        }else if(navigationType == UIWebViewNavigationTypeOther){
        // FaceBookページか?
            if ([self isFacebookURL:[request URL]]) {
                [self fbShare];
                return NO;
            }
        }
    }
    return YES;
}


- (BOOL)isTwitterURL:(NSURL *)url
{
    NSString *urlString = [url absoluteString];
    NSString *twitterUrlString = @"https://twitter.com";
    
    // Twitterページか?
    NSRange range = [urlString rangeOfString:twitterUrlString];
    if (range.location != NSNotFound) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isFacebookURL:(NSURL *)url
{
    // facebookページか?
    NSString *urlString = [url absoluteString];
    NSString *facebookrUrlString = @"https://m.facebook.com/login.php";
    NSRange rangefb = [urlString rangeOfString:facebookrUrlString];
    if (rangefb.location != NSNotFound) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark -- ナヴィゲーションバーを隠す処理の実装 --

-(void)navBarToolBarVisibleHide{
    self.menu = (AwesomeMenu*)[self.view viewWithTag:1];
    self.beginScrollOffsetY = -1;
    [UIView animateWithDuration:0.2 animations:^{
        CGRect rect = self.toolBar.frame;
        self.toolBar.frame = CGRectMake(rect.origin.x,
                                        rect.origin.y + rect.size.height,
                                        rect.size.width,
                                        rect.size.height);
        
        CGSize frameSize = self.view.bounds.size;
        self.menu.startPoint = CGPointMake(frameSize.width - 25, frameSize.height + 22.5f);
        
    } completion:^(BOOL finished) {
        self.toolBar.hidden = YES;
        self.menu.hidden = YES;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }];
    
   
}
-(void)navBarToolBarVisibleDisplay{
    self.menu = (AwesomeMenu*)[self.view viewWithTag:1];
    self.beginScrollOffsetY = -1;
    self.toolBar.hidden = NO;
    self.menu.hidden = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        
        CGRect rect = self.toolBar.frame;
        self.toolBar.frame = CGRectMake(rect.origin.x,
                                        rect.origin.y + rect.size.height,
                                        rect.size.width,
                                        rect.size.height);
        CGSize frameSize = self.view.bounds.size;
        self.menu.startPoint = CGPointMake(frameSize.width - 25, frameSize.height + 22.5f);
        
        
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        
        
        frameSize = self.view.bounds.size;
        self.menu.startPoint = CGPointMake(frameSize.width - 25, frameSize.height - 22.5f);
        } completion:^(BOOL finished) {
    }];
}



-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    self.beginScrollOffsetY = [scrollView contentOffset].y;
   //NSLog(@"-------");
    NSLog(@"%FRAME:lf",self.view.center.y);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
  if(self.beginScrollOffsetY != -1){
      
        if(!self.navigationController.navigationBarHidden && (self.beginScrollOffsetY < [scrollView contentOffset].y)){
            [self navBarToolBarVisibleHide];
        }else if(0.0 != self.beginScrollOffsetY && self.navigationController.navigationBarHidden &&(self.beginScrollOffsetY > [scrollView contentOffset].y)){
            [self navBarToolBarVisibleDisplay];
        }
      
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
