//
//  FavoriteTableViewController.m
//  omoro
//
//  Created by TakafumiYANO on 2014/08/09.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "FavoriteTableViewController.h"
#import "CenterTableViewCell.h"
#import "FavoriteFeedData.h"
#import "AppDelegate.h"
#import "WebVC.h"
#import "FeedInfo.h"

@interface FavoriteTableViewController ()
@property(strong, nonatomic)NSMutableArray *allFavoriteData;
@property(strong, nonatomic)FavoriteFeedData *currentItem;
@property(strong,nonatomic)AppDelegate *appDelegate;
@end

@implementation FavoriteTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.allFavoriteData = [[NSMutableArray alloc]initWithArray:[FavoriteFeedData MR_findAll]];
    self.tableView.rowHeight = 70.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tapEditButton:(id)sender
{
    if(self.tableView.editing){
        [self.tableView setEditing:NO animated:YES];
    }else{
        [self.tableView setEditing:YES animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.allFavoriteData count];
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 70.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenterCell" forIndexPath:indexPath];
	if (!cell) {
		cell = [[CenterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CenterCell"];
	}
    if ( (indexPath.row) < [self.allFavoriteData count]){
        self.currentItem = self.allFavoriteData[indexPath.row];
        cell.titleLabel.text = self.currentItem.title;
        cell.mediaLabel.text = self.currentItem.media;
        cell.dateLabel.text = [self nsdateToNsstring:self.currentItem.created];
       // [cell.wineMainImg sd_setImageWithURL:[NSURL URLWithString:self.currentItem.img_url] placeholderImage:nil];
    }
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.currentItem = self.allFavoriteData[indexPath.row];
    [self performSegueWithIdentifier:@"favoriteToWeb" sender:self];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",indexPath);
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //cellの削除
        FavoriteFeedData *delData = [self.allFavoriteData objectAtIndex:indexPath.row];
        
        NSLog(@"%@",self.currentItem.title);
        [self.allFavoriteData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        //recordの削除
        
        NSArray *c =[FeedInfo MR_findByAttribute:@"feed_id" withValue:delData.uid];
        FeedInfo *cl = [c objectAtIndex:0];
        cl.isfavorite = [NSNumber numberWithBool:NO];
        
        [delData MR_deleteEntity];
        self.appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = self.appDelegate.context;
        [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            NSLog(@"success=%d, error=%@",success,error);
        }];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (NSString *)nsdateToNsstring:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy/MM/dd HH:mm:ss"];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"favoriteToWeb"] ) {
        WebVC *mWebVC = [segue destinationViewController];
        mWebVC.urlStr = self.currentItem.link;
        mWebVC.mode = 1;
        mWebVC.currentPageDic = [NSDictionary dictionaryWithObjectsAndKeys:self.currentItem.uid, @"id", nil];
    }
}

@end
