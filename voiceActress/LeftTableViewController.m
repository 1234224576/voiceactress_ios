//
//  LeftTableViewController.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "LeftTableViewController.h"
#import "UIViewController+JASidePanel.h"
#import "JASidePanelController.h"

@interface LeftTableViewController ()<UITableViewDelegate>
@property (nonatomic,strong) WSCoachMarksView *coachView;
@end

@implementation LeftTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    
    //タイトル画像設定
//    UIImageView *navigationTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"omoro_sl_logo"]];
//    navigationTitle.frame = CGRectMake(0, 0, 320, 44);
//    self.navigationItem.titleView = navigationTitle;
    [self setUpHelp];
}


-(CGFloat)tableView:(UITableView*)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45;
}

#pragma mark ヘルプ関連
- (void)setUpHelp
{
    NSArray *marks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,0},{220,45}}],
                           @"caption": @"全声優の新着記事を\t\t\t\t\n表示します\t\t\t"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,45},{220,135}}],
                           @"caption": @"人気記事\t\t\t\t\nを表示します\t\t\t"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,180},{220,45}}],
                           @"caption": @"登録した声優の記事\t\t\t\t\nを全て表示します\t\t\t\t"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,225},{220,45}}],
                           @"caption": @"登録した声優を\t\t\t\t\n表示,編集します\t\t\t\t\n各声優の記事の閲覧\t\t\t\t\nもできます\t\t\t\t"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,270},{220,45}}],
                           @"caption": @"お気に入り登録した\t\t\t\t\n記事を表示します\t\t\t\t"
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0,315},{220,45}}],
                           @"caption": @"声優名で検索できます\t\t\t\t\n"
                           },
                       ];
    
    self.coachView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:marks];
    self.coachView.maxLblWidth = 400.0f;
    [self.view addSubview:self.coachView];
}
- (void)viewDidAppear:(BOOL)animated {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"MenuHelp"])
    {
        // 初回起動時
        [self.coachView start];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MenuHelp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	
	// 通知を送信
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:kLeftViewDidSelectedCellNotification
					  object:self
					userInfo:@{kSelectCellRowTitleUserInfoKey : [self __cellTitleAtRow:indexPath.row]}];
	
	// ここでセンターを表示
	[self.navigationController.sidePanelController showCenterPanelAnimated:YES];
}


/****************************
 *
 * Privarte Methods
 *
 ****************************/
#pragma mark -
#pragma mark ===== Privarte Methods =====
- (NSString*)__cellTitleAtRow:(NSInteger)row
{
	return [@(row+1) stringValue];
}


@end
