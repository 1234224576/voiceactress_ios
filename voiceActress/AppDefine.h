//
//  AppDefine.h
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//


/// 左パネルのCellが選択された際に通知するキー
#define kLeftViewDidSelectedCellNotification			@"LeftViewDidSelectedCell"

/// 左パネルのCellが選択された際に通知するキー
#define kCenterViewDidSelectedCellNotification			@"CenterViewDidSelectedCell"

/// NSNotificationで渡されるUserInfoのDictionaryキー
#define kSelectCellRowTitleUserInfoKey					@"SelectCellRowTitleUserInfoKey"

/// NSNotificationで渡されるUserInfoのDictionaryキー
#define kPanGEsture					@"PanGesture"

//GAのユーザID
#define GOOGLE_ANALYTICS_ID @"UA-52770108-1"

//in-mobiの設定
#define INMOBIAD_ID @"190e94545d6e49e0ae3bfd68237f3d78"

//APPCLOUD
#define APPCLOUD_ID @"724b75532679234d03db6d550cd1f44e4f31f319"

//広告の数
#define ADNUM 5

//crashLyticsの設定
#define CRASHLYTICS_ID @"871e5323305cc35271f611c2b0be427a8d048cd4"

//appStoreID
#define APP_ID_APP_STORE @"923688074"
