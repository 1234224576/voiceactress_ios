//
//  AppDelegate.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/11.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import "AppDelegate.h"
#import <Crashlytics/Crashlytics.h>
#import "SHKConfiguration.h"
#import "MySHKConfigurator.h"
#import "SHKFacebook.h"
#import "SHKTwitter.h"
#import "InMobi.h"
#import "IMConstants.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // initialization: Google Analytics
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;
    gai.dispatchInterval = 5;
    [[gai logger] setLogLevel:kGAILogLevelError]; // ログレベルを変えることができる
    [gai trackerWithTrackingId:GOOGLE_ANALYTICS_ID];
    
    [Crashlytics startWithAPIKey:CRASHLYTICS_ID];
    
    // ShareKitのインスタンス化
    DefaultSHKConfigurator *configurator = [[MySHKConfigurator alloc] init];
    [SHKConfiguration sharedInstanceWithConfigurator:configurator];

    
    //Magical-RecordCoreDataセットアップ
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"omoro_db.sqlite"];
    self.context = [NSManagedObjectContext MR_defaultContext];
//    in-mobiの設定
//    [InMobi initialize:INMOBIAD_ID];
//    [InMobi setLogLevel:IMLogLevelVerbose];
//    [InMobi setLanguage:@"jpn"];
//    [InMobi setLocationWithCity:@"Tokyo" state:nil country:@"JPN"];

    //AppCloudセットアップ
    [appCCloud setDelegate:self];
    [appCCloud setupAppCWithMediaKey:APPCLOUD_ID option:APPC_CLOUD_AD];

    //PartyTrackingのセットアップ
    [[Partytrack sharedInstance] startWithAppID:3250 AndKey:@"c9302acac898473570bcf8b5da57ae1a"];
    
    //ヘッダー変更
   // [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"om_header"] forBarPosition:UIBarPositionTop barMetrics:UIBarMetricsDefault];
    
    return YES;
}

- (void)initUserDefalult
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    [defaults setObject:@NO forKey:@"permitCahceFun"];
    [defaults setObject:@NO forKey:@"permitImageListFun"];
    [ud registerDefaults:defaults];
}


// appC Cloudの初期化完了時に呼ばれるメソッド
-(void)finishedSetupAppC:(BOOL)succeed
{
    // リリースステータスを取得
    appCReleaseStatus status = [appCCloud getReleaseStatus];
    
    switch (status) {
        case appCReleaseStatusInDevelopment:
            NSLog(@"%ld:開発中", status);
            break;
        case appCReleaseStatusInReview:
            NSLog(@"%ld:公開申請中", status);
            break;
        case appCReleaseStatusReadyForSale:
            NSLog(@"%ld:公開中", status);
            break;
    }
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [SHKFacebook handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Save data if appropriate
    [SHKFacebook handleWillTerminate];
    [MagicalRecord cleanUp];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSString* scheme = [url scheme];
    if ([scheme hasPrefix:[NSString stringWithFormat:@"fb%@", SHKCONFIG(facebookAppId)]]) {
        return [SHKFacebook handleOpenURL:url];
    }
    
    return YES;
}

@end
