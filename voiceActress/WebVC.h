//
//  WebVC.h
//  omoro
//
//  Created by TakafumiYANO on 2014/07/12.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WebVC : GAITrackedViewController
@property(strong, nonatomic)NSDictionary *currentPageDic;
@property(strong, nonatomic)NSString *urlStr;
@property(nonatomic)NSInteger mode;//0 通常, 1favorite, 2キャッシュ, 3規約等, 4検索

@end
