//
//  main.m
//  omoro
//
//  Created by TakafumiYANO on 2014/07/11.
//  Copyright (c) 2014年 Cellars,INC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
